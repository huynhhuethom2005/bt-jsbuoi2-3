/*  
BT1: 
- input: Số ngày
- output: Số tiền lương
- processing: 
+ Gán biến luongNgay = 100
+ Tạo biến soNgay để người dùng nhập
+ Tạo biến tongLuong = luongNgay * soNgay
*/

function tinhLuong() {
  var tienLuong = document.getElementById("tienLuong").value * 1;
  var ngayLuong = document.getElementById("ngayLuong").value * 1;
  var tongLuong = tienLuong * ngayLuong;
  document.getElementById("tongLuong").innerHTML = tongLuong;
}

/*
BT2: 
- input: 5 số nguyên
- output: giá trị trung bình của 5 số
- processing:
+ Tạo 5 biến 
+ Tạo biến trungBinh = (so1+so2+so3+so4+so5)/5
*/

function tinhTrungBinh() {
  var so1 = document.getElementById("so1").value * 1,
    so2 = document.getElementById("so2").value * 1,
    so3 = document.getElementById("so3").value * 1,
    so4 = document.getElementById("so4").value * 1,
    so5 = document.getElementById("so5").value * 1;
  var trungBinh = (so1 + so2 + so3 + so4 + so5) / 5;
  document.getElementById("trungBinh").innerHTML = trungBinh;
}

/*
BT3: 
- input: Số tiền USD
- output: Số tiền VND
- processing: 
+ tạo biến giaUSD = 23500, nhapUSD
+ Nhân USD và nhapUSD
*/

function tinhUSD() {
  const GIAUSD = 23500;
  var sotienUSD = document.getElementById("sotienUSD").value * 1;
  var quydoiVND = GIAUSD * sotienUSD;
  document.getElementById("resultUSD").innerHTML = quydoiVND;
}

/* 
BT4:
- input: chiều dài, chiều rộng
- output: chu vi, diện tích
- processing: tính theo công thức chi vi và diện tích
*/

function tinhCVDT() {
  var chieuDai = document.getElementById("chieuDai").value * 1,
    chieuRong = document.getElementById("chieuRong").value * 1;
  var chuVi = (chieuDai + chieuRong) * 2;
  var dienTich = chieuDai * chieuRong;
  var result = `Chu vi là: ${chuVi}, Diện tích là: ${dienTich}`;
  document.getElementById("resultCVDT").innerHTML = result;
}

/*
BT5: 
- input: số có 2 chữ số
- output: tổng của 2 ký số
- processing:
+ Chia lấy phần dư với 10 để lấy hàng đơn vị
+ Dùng math.floor để lấy hàng chục
*/

function tinhTong() {
  var soNguyen = document.getElementById("soNguyen").value * 1;
  var donVi = soNguyen % 10;
  var hangChuc = Math.floor(soNguyen / 10);
  var Tong = donVi + hangChuc;
  document.getElementById('resultTinhTong').innerHTML = Tong
}
